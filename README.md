## Environmental variables

You will need to setup the following variables in your local `.env`:

    MAIL_MAILER=smtp
    MAIL_HOST=smtp.mailtrap.io
    MAIL_PORT=2525
    MAIL_USERNAME=<username>
    MAIL_PASSWORD=<password>
    
    RAPID_API_KEY=<api key>
