<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class HistoricalDataMailable extends Mailable
{
    use Queueable, SerializesModels;

    public $company_name;
    public $company_symbol;
    public $prices;
    public $start_date;
    public $end_date;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(string $company_symbol, string $company_name, string $start_date, string $end_date, array $prices)
    {
        $this->company_symbol = $company_symbol;
        $this->company_name   = $company_name;
        $this->start_date     = $start_date;
        $this->end_date       = $end_date;
        $this->prices         = $prices;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject($this->company_name)->view('emails.result');
    }
}
