<?php

namespace App\Providers;

use App\Services\NasdaqCompanyDataService;
use Illuminate\Support\ServiceProvider;

class NasdaqCompanyDataServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(NasdaqCompanyDataService::class, function ($app) {
            return new NasdaqCompanyDataService();
        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
