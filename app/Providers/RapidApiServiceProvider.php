<?php

namespace App\Providers;

use App\Services\RapidApiService;
use Illuminate\Support\ServiceProvider;

class RapidApiServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(RapidApiService::class, function ($app) {
            $api_key = config('rapid_api.api_key');

            if (empty($api_key)) {
                throw new \InvalidArgumentException('rapid_api.api_key is missing');
            }

            return new RapidApiService($api_key);
        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
