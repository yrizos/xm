<?php


namespace App\Services;

use GuzzleHttp\Client;

interface ApiServiceInterface
{

    public function getClient(): Client;

}
