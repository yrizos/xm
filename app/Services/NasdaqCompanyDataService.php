<?php

namespace App\Services;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;

class NasdaqCompanyDataService implements ApiServiceInterface
{
    const ENDPOINT = 'https://pkgstore.datahub.io';

    private $companies = NULL;

    public function getClient(): Client
    {
        return new Client([
            'base_uri' => self::ENDPOINT,
        ]);
    }

    /**
     * @throws ClientException
     * @throws \RuntimeException
     */
    public function getCompanies(): array
    {
        if (!is_null($this->companies)) {
            return $this->companies;
        }

        $uri      = '/core/nasdaq-listings/nasdaq-listed_json/data/a5bc7580d6176d60ac0b2142ca8d7df6/nasdaq-listed_json.json';
        $response = $this->getClient()->request('GET', $uri);
        $body     = $response->getBody();
        $body     = !empty($body) ? json_decode($body, TRUE) : NULL;

        if (empty($body)) {
            throw new \RuntimeException('Could not fetch company data.');
        }

        $body = array_filter($body, function ($item) {
            return
                is_array($item)
                && isset($item['Company Name'])
                && isset($item['Financial Status'])
                && isset($item['Market Category'])
                && isset($item['Round Lot Size'])
                && isset($item['Security Name'])
                && isset($item['Symbol'])
                && isset($item['Test Issue']);
        });

        $this->companies = array_values($body);

        return $this->companies;
    }

    public function findCompany(string $company_symbol): ?array
    {
        $companies = $this->getCompanies();

        foreach ($companies as $item) {
            if ($item['Symbol'] == $company_symbol) {
                return $item;
            }
        }

        return NULL;
    }

}
