<?php

namespace App\Services;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;

class RapidApiService implements ApiServiceInterface
{

    const HOST    = 'apidojo-yahoo-finance-v1.p.rapidapi.com';
    const VERSION = 'v2';

    private $api_key = '';

    public function __construct(string $api_key)
    {
        $this->setApiKey($api_key);
    }

    public function setApiKey(string $api_key): self
    {
        $this->api_key = $api_key;

        return $this;
    }

    public function getApiKey(): string
    {
        return $this->api_key;
    }

    public function getClient(): Client
    {
        return new Client([
            'base_uri' => 'https://' . self::HOST,
            'headers'  => [
                'x-rapidapi-host' => self::HOST,
                'x-rapidapi-key'  => $this->getApiKey(),
            ],
        ]);
    }

    /**
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \InvalidArgumentException
     * @throws \RuntimeException
     */
    public function getHistoricalData(string $symbol, int $period1, int $period2, string $frequency = '1d', string $filter = 'history'): array
    {
        $symbol = trim($symbol);

        if (empty($symbol)) {
            throw new \InvalidArgumentException('Symbol is invalid');
        }

        if (!in_array($frequency, ['1d', '1wk', '1mo'])) {
            throw new \InvalidArgumentException('Frequency is invalid');
        }

        if (!in_array($filter, ['history', 'div', 'split'])) {
            throw new \InvalidArgumentException('Filter is invalid');
        }

        $uri      = '/stock/' . self::VERSION . '/get-historical-data';
        $response = $this->getClient()->request(
            'GET',
            $uri,
            [
                'query' => [
                    'frequency' => $frequency,
                    'filter'    => $filter,
                    'period1'   => $period1,
                    'period2'   => $period2,
                    'symbol'    => $symbol,
                ],
            ]
        );

        $body = $response->getBody();


        $body = !empty($body) ? json_decode($body, TRUE) : NULL;

        if (!is_array($body)) {
            throw new \RuntimeException('Api response is invalid');
        }

        return $body;
    }

}
