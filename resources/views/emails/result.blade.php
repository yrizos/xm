<p><strong>Historical data for {{$company_symbol}}, from {{$start_date}} to {{$end_date}}.</strong><p>

<hr />

<table cellpadding="15" cellspacing="0" border="1">

    <thead>
    <tr>
        <th scope="col">Date</th>
        <th scope="col">Open</th>
        <th scope="col">High</th>
        <th scope="col">Low</th>
        <th scope="col">Close</th>
        <th scope="col">Volume</th>
    </tr>
    </thead>

    <tbody>

    @foreach ($prices as $item)
        <tr>
            <td>{{$item['date']}}</td>
            <td>{{$item['open']}}</td>
            <td>{{$item['high']}}</td>
            <td>{{$item['low']}}</td>
            <td>{{$item['close']}}</td>
            <td>{{$item['volume']}}</td>
        </tr>
    @endforeach

    </tbody>
</table>

