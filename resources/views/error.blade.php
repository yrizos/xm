@extends('layout')


@section('content')

    @foreach ($errors as $item)

        <div class="alert alert-danger" role="alert">
            {{ $item }}
        </div>

    @endforeach

    <div class="border-top mt-3 pt-3">
        <a href="/" class="btn btn-secondary">Back</a>
    </div>
@endsection()
