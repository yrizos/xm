@extends('layout')


@section('content')

    <h5>Historical data for {{$company['Symbol']}}, from {{$start_date}} to {{$end_date}}</h5>

    <ul class="nav nav-tabs" id="tabs" role="tablist">
        <li class="nav-item">
            <a class="nav-link active" id="table-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Table</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" id="chart-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Chart</a>
        </li>

    </ul>
    <div class="tab-content" id="tabsContent">
        <div class="tab-pane bg-white p-3 fade show active" id="home" role="tabpanel" aria-labelledby="table-tab">

            <table class="table table-bordered">
                <thead>
                <tr>
                    <th scope="col">Date</th>
                    <th scope="col">Open</th>
                    <th scope="col">High</th>
                    <th scope="col">Low</th>
                    <th scope="col">Close</th>
                    <th scope="col">Volume</th>
                </tr>
                </thead>

                <tbody>

                @foreach ($prices as $item)
                    <tr>
                        <td>{{$item['date']}}</td>
                        <td>{{$item['open']}}</td>
                        <td>{{$item['high']}}</td>
                        <td>{{$item['low']}}</td>
                        <td>{{$item['close']}}</td>
                        <td>{{$item['volume']}}</td>
                    </tr>
                @endforeach

                </tbody>
            </table>

        </div>
        <div class="tab-pane bg-white p-3 fade" id="profile" role="tabpanel" aria-labelledby="chart-tab">

            <canvas id="chart"></canvas>

        </div>
    </div>


    <div class="border-top mt-3 pt-3 pb-3">
        <a href="/" class="btn btn-secondary">Back</a>
    </div>
@endsection()

@section('styles')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.3/Chart.min.css"
          integrity="sha512-/zs32ZEJh+/EO2N1b0PEdoA10JkdC3zJ8L5FTiQu82LR9S/rOQNfQN7U59U9BC12swNeRAz3HSzIL2vpp4fv3w=="
          crossorigin="anonymous"/>
@endsection()

@section('scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.3/Chart.min.js"
            integrity="sha512-s+xg36jbIujB2S2VKfpGmlC3T5V2TF3lY48DX7u2r9XzGzgPsa6wTpOQA7J9iffvdeBN0q9tKzRxVxw1JviZPg=="
            crossorigin="anonymous"></script>

    <script>
        var ctx = document.getElementById('chart').getContext('2d');
        var chart = new Chart(ctx, {
            type: 'line',
            data: {
                labels: {!! json_encode($labels) !!},
                datasets: [
                    {
                        label: 'Open',
                        borderColor: "#28a745",
                        fill: false,
                        data: {!! json_encode($open) !!}
                    },
                    {
                        label: 'Close',
                        borderColor: "#dc3545",
                        fill: false,
                        data: {!! json_encode($close) !!}
                    }
                ]
            },

            // Configuration options go here
            options: {}
        });
    </script>
@endsection()
