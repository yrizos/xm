@extends('layout')

@section('title', 'Welcome')

@section('content')

    <form method="post" action="/" class="needs-validation " novalidate>
    @csrf <!-- {{ csrf_field() }} -->

        <div class="form-group row">
            <label for="company_symbol" class="col-sm-2 col-form-label">Company Symbol:</label>
            <div class="col-sm-10">

                <select class="custom-select" name="company_symbol">
                    @foreach ($companies as $item)
                        <option value="{{ $item['Symbol'] }}">{{ $item['Symbol'] }}
                            | {{ $item['Company Name'] }}</option>
                    @endforeach

                </select>
            </div>

        </div>


        <div class="form-group row">
            <label for="start_date" class="col-sm-2 col-form-label">Start Date:</label>
            <div class="col-sm-10">
                <input type="date" class="form-control" id="start_date" name="start_date"
                       min="{{ $date_min }}" required/>
            </div>
        </div>
        <div class="form-group row">
            <label for="end_date" class="col-sm-2 col-form-label">End Date:</label>
            <div class="col-sm-10">
                <input type="date" class="form-control" id="end_date" name="end_date"
                       min="{{ $date_min }}" required/>
            </div>
        </div>

        <div class="form-group row">
            <label for="email" class="col-sm-2 col-form-label">e-mail:</label>
            <div class="col-sm-10">
                <input type="email" class="form-control" id="email" name="email" required/>
            </div>
        </div>

        <div class="form-group row">
            <div class="col-sm-10 offset-sm-2">
                <button type="submit" class="btn btn-primary">Submit</button>
            </div>
        </div>
    </form>

@endsection()

@section('scripts')


    <script>
        $("#start_date").on("change", function () {

            let start_date = $(this);
            let start_date_timestamp = Date.parse(start_date.val());
            let end_date_min = new Date(start_date_timestamp);
            let end_date = $('#end_date');

            $(end_date).attr('min', end_date_min.toISOString().split('T')[0]);

            if ($(end_date).val()) {
                end_date_timestamp = Date.parse($(end_date).val());

                if(end_date_timestamp < start_date_timestamp) {
                    $(end_date).val('');
                }
            }
        });

    </script>

@endsection()
