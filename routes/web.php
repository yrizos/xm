<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function (\App\Services\NasdaqCompanyDataService $nasdaq) {
    return view('welcome', [
        'companies' => $nasdaq->getCompanies(),
        'date_min'  => (new \Carbon\Carbon())->format('Y-m-d'),
    ]);
});

Route::post('/', function (\App\Services\NasdaqCompanyDataService $nasdaq, \App\Services\RapidApiService $rapid_api, \Illuminate\Http\Request $request) {
    $symbol     = (string)$request->get('company_symbol');
    $company    = !empty($symbol) ? $nasdaq->findCompany($symbol) : NULL;
    $start_date = \Carbon\Carbon::createFromFormat('Y-m-d', (string)$request->get('start_date'))->setTime(0, 0, 0, 0);
    $end_date   = \Carbon\Carbon::createFromFormat('Y-m-d', (string)$request->get('end_date'))->setTime(0, 0, 0, 0);
    $email      = filter_var($request->get('email'), FILTER_VALIDATE_EMAIL) ? filter_var($request->get('email', FILTER_SANITIZE_EMAIL)) : NULL;

    $errors = [];

    if (empty($company)) {
        $errors[] = 'Company symbol ' . $symbol . ' is invalid.';
    }

    $now = \Carbon\Carbon::now()->setTime(0, 0, 0, 0);
    if (empty($start_date) || $start_date->lessThan($now)) {
        $errors[] = 'Start date is invalid.';
    }

    if (empty($end_date) || $end_date->lessThan($start_date)) {
        $errors[] = 'End date is invalid.';
    }

    if (empty($email)) {
        $errors[] = 'e-mail is invalid.';
    }

    $historical_data = $rapid_api->getHistoricalData(
        $symbol,
        $start_date->getTimestamp(),
        $end_date->getTimestamp()
    );

    if (empty($historical_data)) {
        $errors[] = 'No historical data found for ' . $symbol;
    }

    if (!empty($errors)) {
        return view('error', ['errors' => $errors]);
    }

    $prices = [];
    $labels = [];
    $open   = [];
    $close  = [];

    foreach ($historical_data['prices'] as $item) {
        $item['date'] = (new \Carbon\Carbon($item['date']))->format('Y-m-d');

        $labels[] = $item['date'];
        $open[]   = $item['open'];
        $close[]  = $item['close'];

        $prices[] = $item;
    }


    \Illuminate\Support\Facades\Mail::to($email)->send(new \App\Mail\HistoricalDataMailable(
        $company['Symbol'],
        $company['Company Name'],
        $start_date->format('Y-m-d'),
        $end_date->format('Y-m-d'),
        $prices
    ));


    return view('result', [
        'company'    => $company,
        'start_date' => $start_date->format('Y-m-d'),
        'end_date'   => $end_date->format('Y-m-d'),
        'prices'     => $prices,
        'labels'     => $labels,
        'open'       => $open,
        'close'      => $close,
    ]);
});
