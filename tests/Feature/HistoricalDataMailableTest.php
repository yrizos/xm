<?php

use Tests\TestCase;
use \App\Mail\HistoricalDataMailable;

class HistoricalDataMailableTest extends TestCase
{
    public function testHistoricalDataMail()
    {
        $company_symbol = 'AAIT';
        $company_name   = 'iShares MSCI All Country Asia Information Technology Index Fund';
        $start_date     = '2020-07-23';
        $end_date       = '2020-07-25';
        $prices         = [];

        $mail = new HistoricalDataMailable($company_symbol, $company_name, $start_date, $end_date, $prices);

        $this->assertEquals($company_symbol, $mail->company_symbol);
        $this->assertEquals($company_name, $mail->company_name);
        $this->assertEquals($start_date, $mail->start_date);
        $this->assertEquals($end_date, $mail->end_date);
        $this->assertEquals($prices, $mail->prices);

        $mail->build();

        $this->assertEquals($company_name, $mail->subject);
    }
}
