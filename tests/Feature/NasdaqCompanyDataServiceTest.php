<?php

use Tests\TestCase;
use App\Services\NasdaqCompanyDataService;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Response;

class NasdaqCompanyDataServiceTest extends TestCase
{
    private $companies
        = [
            [
                "Company Name"     => "iShares MSCI All Country Asia Information Technology Index Fund",
                "Financial Status" => "N",
                "Market Category"  => "G",
                "Round Lot Size"   => "100",
                "Security Name"    => "iShares MSCI All Country Asia Information Technology Index Fund",
                "Symbol"           => "AAIT",
                "Test Issue"       => "N",
            ],
            [
                "Company Name"     => "American Airlines Group, Inc.",
                "Financial Status" => "N",
                "Market Category"  => "Q",
                "Round Lot Size"   => "100",
                "Security Name"    => "American Airlines Group, Inc. - Common Stock",
                "Symbol"           => "AAL",
                "Test Issue"       => "N",
            ],
            [
                "Company Name"     => "Atlantic American Corporation",
                "Financial Status" => "N",
                "Market Category"  => "G",
                "Round Lot Size"   => "100",
                "Security Name"    => "Atlantic American Corporation - Common Stock",
                "Symbol"           => "AAME",
                "Test Issue"       => "N",
            ],

        ];

    private function getMockService()
    {
        $mock_client   = Mockery::mock(Client::class)->makePartial();
        $mock_response = new Response(200, [], json_encode($this->companies));

        $mock_client->shouldReceive('request')->andReturn($mock_response);

        $mock_service = Mockery::mock(NasdaqCompanyDataService::class)->makePartial();
        $mock_service->shouldReceive('getClient')->andReturn($mock_client);

        return $mock_service;
    }

    public function testGetCompanies()
    {
        $mock_service = $this->getMockService();
        $result       = $mock_service->getCompanies();

        $this->assertEquals($this->companies, $result);
    }

    public function testFindCompany()
    {
        $mock_service = $this->getMockService();

        foreach ($this->companies as $item) {

            $result = $mock_service->findCompany($item['Symbol']);

            $this->assertEquals($item, $result);
        }


        $result = $mock_service->findCompany('');

        $this->assertNull($result);
    }

    public function testGetClient()
    {
        $client = (new NasdaqCompanyDataService())->getClient();

        /** @var GuzzleHttp\Psr7\Uri $base_uri */
        $base_uri = $client->getConfig('base_uri');
        $endpoint = $base_uri->getScheme() . '://' . $base_uri->getHost();

        $this->assertEquals(NasdaqCompanyDataService::ENDPOINT, $endpoint);
    }

}
