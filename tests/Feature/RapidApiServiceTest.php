<?php

use Tests\TestCase;
use App\Services\RapidApiService;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Response;

class RapidApiServiceTest extends TestCase
{
    private $api_key = 'api_key';
    private $response
                     = [
            "prices"         => [
                [
                    "date"     => 1562074200,
                    "open"     => 20.790000915527,
                    "high"     => 22.450000762939,
                    "low"      => 20.200000762939,
                    "close"    => 22.370000839233,
                    "volume"   => 42111300,
                    "adjclose" => 22.370000839233,
                ],
                [
                    "date"     => 1561987800,
                    "open"     => 19.770000457764,
                    "high"     => 19.860000610352,
                    "low"      => 19.170000076294,
                    "close"    => 19.239999771118,
                    "volume"   => 4669400,
                    "adjclose" => 19.239999771118,
                ],
                [
                    "date"     => 1561728600,
                    "open"     => 18.799999237061,
                    "high"     => 19.430000305176,
                    "low"      => 18.729999542236,
                    "close"    => 19.389999389648,
                    "volume"   => 4413700,
                    "adjclose" => 19.389999389648,
                ],
                [
                    "date"     => 1561642200,
                    "open"     => 18.280000686646,
                    "high"     => 18.75,
                    "low"      => 18.180000305176,
                    "close"    => 18.719999313354,
                    "volume"   => 3267300,
                    "adjclose" => 18.719999313354,
                ],
                [
                    "date"     => 1561555800,
                    "open"     => 18.430000305176,
                    "high"     => 18.450000762939,
                    "low"      => 17.85000038147,
                    "close"    => 18.290000915527,
                    "volume"   => 4014100,
                    "adjclose" => 18.290000915527,
                ],
            ],
            "isPending"      => FALSE,
            "firstTradeDate" => 733674600,
            "id"             => "1d15464484001562086800",
            "timeZone"       => [
                "gmtOffset" => -14400,
            ],
            "eventsData"     => [],
        ];

    public function testGetHistoricalData()
    {
        $mock_client   = Mockery::mock(Client::class)->makePartial();
        $mock_response = new Response(200, [], json_encode($this->response));

        $mock_client->shouldReceive('request')->andReturn($mock_response);

        $mock_service = Mockery::mock(RapidApiService::class)->makePartial();
        $mock_service->shouldReceive('getClient')->andReturn($mock_client);
        $result = $mock_service->getHistoricalData('AAIT', 1, 2);

        $this->assertEquals($this->response, $result);
    }

    public function testGetHistoricalData_emptySymbol()
    {
        $this->expectException(\InvalidArgumentException::class);

        $service = new RapidApiService($this->api_key);

        $service->getHistoricalData(' ', 1, 2);
    }

    public function testGetHistoricalData_invalidFrequency()
    {
        $this->expectException(\InvalidArgumentException::class);

        $service = new RapidApiService($this->api_key);

        $service->getHistoricalData('AAIT', 1, 2, 'invalid');
    }

    public function testGetHistoricalData_invalidFilter()
    {
        $this->expectException(\InvalidArgumentException::class);

        $service = new RapidApiService($this->api_key);

        $service->getHistoricalData('AAIT', 1, 2, '1d', 'invalid');
    }

    public function testGetHistoricalData_invalidApiResponse()
    {
        $this->expectException(\RuntimeException::class);

        $mock_client   = Mockery::mock(Client::class)->makePartial();
        $mock_response = new Response(200, [], NULL);

        $mock_client->shouldReceive('request')->andReturn($mock_response);

        $mock_service = Mockery::mock(RapidApiService::class)->makePartial();
        $mock_service->shouldReceive('getClient')->andReturn($mock_client);

        $mock_service->getHistoricalData('AAIT', 1, 2);
    }

    public function testGetClient()
    {
        $client = (new RapidApiService($this->api_key))->getClient();

        /** @var GuzzleHttp\Psr7\Uri $base_uri */
        $base_uri = $client->getConfig('base_uri');

        $this->assertEquals(RapidApiService::HOST, $base_uri->getHost());
        $this->assertEquals('https', $base_uri->getScheme());

        /** @var array $headers */
        $headers = $client->getConfig('headers');

        $this->assertArrayHasKey('x-rapidapi-host', $headers);
        $this->assertEquals(RapidApiService::HOST, $headers['x-rapidapi-host']);
        $this->assertArrayHasKey('x-rapidapi-key', $headers);
        $this->assertEquals($this->api_key, $headers['x-rapidapi-key']);
    }


}
